﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class uiManager : MonoBehaviour {

    public Button[] buttons;
    bool paused = false;
    public Text scoreText;
    public int score;
    bool gameOver;
    carSpawnPosition carScore;
    //public carSpawnPosition enemy;
    //public carController redCar;



	void Start () {
        gameOver = false;
        score = 0;
        InvokeRepeating("scoreUpdate", 1.0f, 0.5f);
		
	}


 
    void Update()
    {
        scoreText.text = "Score: " + score;
        

        //if (Input.GetKeyDown(KeyCode.P))
        //{

        //    if (paused)
        //    {

        //        Time.timeScale = 1;
        //        paused = false;

        //    }
        //    else 
        //    {
        //        Time.timeScale = 0;
        //        paused = true;


        //    }

        //}
    }

    void scoreUpdate()
    {
        if (!gameOver)
        {

            carScore.scoreDetect();

        }

    }

    public void gameOverActivated()
    {
        gameOver = true;
        foreach(Button button in buttons)
        {

            button.gameObject.SetActive(true);
        }
    }

    public void Pause()
    {

        if (Time.timeScale == 1)

        {
            Time.timeScale = 0;

        } else if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            
        } }

  public void Play()
    {
        Application.LoadLevel("Level1");
        //SceneManager.LoadScene("Level1");


    }

    public void Replay()
    {

        Application.LoadLevel(Application.loadedLevel);

    }

    public void Menu()
    {
        Application.LoadLevel("menuScene");

    }

    public void Exit() {

        Application.Quit();
    }





    }

