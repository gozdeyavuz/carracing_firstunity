﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carController : MonoBehaviour {

    public float carSpeed;
    Vector3 position;
    public float maxPos;
    public uiManager ui;
    public audioManager aManager;

    private void Awake()
    {
        aManager.carSound.Play();
    }

    void Start () {
       
        position = transform.position;
        


    }
	
	
	void Update () {
        position.x += Input.GetAxis("Horizontal") * carSpeed * Time.deltaTime;
        position.x = Mathf.Clamp(position.x, -2.2f, 2.2f);
        transform.position = position;

	}

    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "enemyCar")
        {

            Destroy(gameObject);
            ui.gameOverActivated();
            aManager.carSound.Stop();

        }
    }

}
