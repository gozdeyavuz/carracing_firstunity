﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carSpawnPosition : MonoBehaviour {

    public GameObject[] cars;
    public GameObject redCar;
    int carNo;
    public float maxPos= 2.2f;
    public float delayTimer=1.0f;
    float timer;
    uiManager ui;
	void Start () {


        timer = delayTimer;

    }

	
	// Update is called once per frame
	void Update () {


        timer -= Time.deltaTime;

        if (timer <= 0)
        {

            Vector3 carPos = new Vector3(Random.Range(-2.2f, maxPos), transform.position.y,transform.position.z);
            carNo = Random.Range(0,6);
            Instantiate(cars[carNo], carPos, transform.rotation);


            

            timer = delayTimer;

        }

        
       
    }

     public void scoreDetect()
    {
        
        if (redCar.transform.localPosition.y == cars[carNo].transform.localPosition.y)
        {   ui.score += 1;

            //return true;
        }

       // return false;
    }       

}
